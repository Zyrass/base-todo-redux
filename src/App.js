import TodoAdd from "./components/TodoAdd";
import TodoFilter from "./components/TodoFilter";
import TodoList from "./components/TodoList";

function App() {
  return (
    <div className="container p-5">
      <h4>Ajouter un todo</h4>
      <hr className="my-4"/>
      <TodoAdd />
      <hr className="my-4"/>
      <div className="card">
        <div className="card-header d-flex flex-row align-items-center">
          <span className="flex-fill">Todo list</span>
          <TodoFilter />
        </div>
      </div>
      <div className="card-body bg-dark">
        <TodoList />
      </div>
    </div>
  );
}

export default App;
