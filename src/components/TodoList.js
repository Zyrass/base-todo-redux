import React from "react"
import TodoItem from "./TodoItem"

const TodoList = props => {
    return(
        <>
            {/* Component liste item  */}
            <div className="list-group">
                <TodoItem />
                <TodoItem />
                <TodoItem />
                <TodoItem />
            </div>
        </>
    )
}

export default TodoList