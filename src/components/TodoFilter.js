import React from "react"

const TodoFilter = (props) => {
    return(
        <>
            {/* Composant pour gérer les 3 filtres */}
            <button className="btn btn-primary mr-2">Tout</button>
            <button className="btn btn-primary mr-2">Fini</button>
            <button className="btn btn-primary">En cours</button>
        </>
    )
}

export default TodoFilter